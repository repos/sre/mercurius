// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")
package server_test

import (
	"encoding/json"
	"errors"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.wikimedia.org/repos/sre/mercurius/server"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// SECTION: Kafka Mocks

// Mock kafka message with timing information
type MockKafkaMessage struct {
	Message kafka.Message
	DelayMS int
}

// Mock kafka consumer implementing the KafkaConsumerApi interface
type MockKafkaConsumer struct {
	Topics   []string
	isOpen   bool
	Messages []MockKafkaMessage
	Assigned *[]kafka.TopicPartition
}

func NewMockKafkaConsumer(messages *[]MockKafkaMessage) *MockKafkaConsumer {
	if messages == nil {
		messages = &[]MockKafkaMessage{}
	}
	return &MockKafkaConsumer{
		Topics:   []string{},
		isOpen:   true,
		Messages: *messages,
		Assigned: &[]kafka.TopicPartition{},
	}
}

func (m *MockKafkaConsumer) Close() error {
	if !m.isOpen {
		return errors.New("consumer is already closed")
	}
	m.isOpen = false
	return nil
}

func (m *MockKafkaConsumer) Subscribe(topic string, rebalanceCb kafka.RebalanceCb) error {
	m.isOpen = true
	assigned := kafka.TopicPartition{
		Topic: &topic,
	}
	m.Assigned = &[]kafka.TopicPartition{assigned}
	m.Topics = append(m.Topics, topic)
	return nil
}

func (m *MockKafkaConsumer) SubscribeTopics(topics []string, rebalanceCb kafka.RebalanceCb) error {
	m.isOpen = true
	assigned := make([]kafka.TopicPartition, len(topics))
	for i, topic := range topics {
		assigned[i] = kafka.TopicPartition{
			Topic: &topic,
		}
	}
	m.Topics = append(m.Topics, topics...)
	return nil
}

func (m *MockKafkaConsumer) Poll(timeoutMS int) kafka.Event {
	if !m.isOpen {
		return nil
	}
	if len(m.Messages) == 0 {
		return nil
	}

	polled := m.Messages[0]

	if polled.DelayMS > timeoutMS {
		time.Sleep(time.Duration(timeoutMS) * time.Millisecond)
		m.Messages[0].DelayMS -= timeoutMS
		return nil
	}
	// Sleep for the delay
	time.Sleep(time.Duration(polled.DelayMS) * time.Millisecond)
	m.Messages = m.Messages[1:]
	return &polled.Message
}

func (m *MockKafkaConsumer) Assignment() ([]kafka.TopicPartition, error) {
	if !m.isOpen {
		return nil, errors.New("consumer is closed")
	}
	if m.Assigned == nil {
		return nil, errors.New("no partitions assigned")
	}
	return *m.Assigned, nil
}

func (m *MockKafkaConsumer) Assign(partitions []kafka.TopicPartition) error {
	if !m.isOpen {
		return errors.New("consumer is closed")
	}
	m.Assigned = &partitions
	return nil
}

func (m *MockKafkaConsumer) Unsubscribe() error {
	if !m.isOpen {
		return errors.New("consumer is closed")
	}
	m.Assigned = nil
	m.Topics = []string{}
	return nil
}

// Section: release mocks
type MockReleaseEvent struct {
	Version server.Version
	Sleep   time.Duration
}

func CreateReleaseFile(path string) error {
	version, err := json.Marshal(server.Version{
		ID: "test",
		// Let's use the typical time between releases of an internal software project
		TimeStamp: time.Now().Add(-8760 * time.Hour),
	})
	if err != nil {
		return err
	}
	return os.WriteFile(path, version, 0644)
}

func ModifyReleaseFile(path string, m ...MockReleaseEvent) error {
	stagePath := path + ".temp"
	for _, e := range m {
		log.Debug().Msgf("waiting for %s before writing a new version to file", e.Sleep)
		time.Sleep(e.Sleep)
		log.Debug().Msgf("Writing version %s to %s", e.Version.ID, path)
		version, err := json.Marshal(e.Version)
		if err != nil {
			return err
		}
		err = os.WriteFile(stagePath, version, 0644)
		if err != nil {
			return err
		}
		err = os.Rename(stagePath, path)
		if err != nil {
			return err
		}
	}
	return nil
}
