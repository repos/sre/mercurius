// Test cases for the server package
package server_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.wikimedia.org/repos/sre/mercurius/server"
)

func TestVersionFromBytes(t *testing.T) {
	mockVersionData := []byte(`{"id":"test","ts":"2024-01-01T00:00:00Z"}`)

	// Test case 1: Test if the version is extracted from the byte string
	version, err := server.VersionFromBytes(mockVersionData)
	if err != nil {
		t.Errorf("Failed to extract version from data: %v", err)
	}
	assert.Equal(t, "test", version.ID)
}

// Test Replaces method
func TestVersionReplaces(t *testing.T) {
	// Test case 1: Test if the version is newer than the current version
	version := server.Version{
		ID:        "test",
		TimeStamp: time.Now(),
	}
	currentVersion := server.Version{
		ID:        "test1",
		TimeStamp: time.Now().Add(-time.Hour),
	}
	assert.True(t, version.Replaces(&currentVersion))

	// Test case 2: Test if the version is newer than the current version
	// but the timestamp is older
	version.TimeStamp = time.Now().Add(-time.Hour * 2)
	assert.False(t, version.Replaces(&currentVersion))

	// Test case 3: the version string is the same
	version.ID = "test1"
	version.TimeStamp = time.Now()
	assert.False(t, version.Replaces(&currentVersion))
}

func TestVersionWatch(t *testing.T) {
	// Test case 1: Test if the version is watched
	version := server.Version{
		ID:        "test",
		TimeStamp: time.Now(),
	}
	versions := []MockReleaseEvent{
		{
			Version: server.Version{
				ID:        "test123",
				TimeStamp: time.Now().Add(1 * time.Hour),
			},
			Sleep: 1 * time.Millisecond,
		},
	}

	path := "/tmp/mercurius-release"
	defer os.Remove(path)
	err := CreateReleaseFile(path)
	assert.Nil(t, err)
	go func() {
		ModifyReleaseFile(path, versions...)
	}()
	err = version.Watch(context.Background(), path)
	assert.Nil(t, err)
}
