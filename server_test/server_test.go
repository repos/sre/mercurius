package server_test

import (
	"os"
	"testing"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"gitlab.wikimedia.org/repos/sre/mercurius/config"
	"gitlab.wikimedia.org/repos/sre/mercurius/server"
	"go.uber.org/atomic"
)

func defaultConfig() *config.Config {
	return &config.Config{
		Brokers:             []string{"localhost:9092"},
		Topics:              []string{"job.topic1", "job.topic2"},
		Group:               "mercurius",
		MaxWorkers:          3,
		ReleaseID:           "apollo-13",
		ReleaseTime:         time.Now(),
		ReleasePath:         "/tmp/mercurius-release",
		Command:             "../fixtures/testJob.sh",
		CommandArgs:         []string{},
		RetryInterval:       1,
		MaxRetries:          2,
		MaxRetryElapsedTime: 0, // No elapsed time limit.
	}
}

func TestMain(m *testing.M) {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	code := m.Run()
	os.Exit(code)
}

// Test creating a new server
func TestMustCreateServer(t *testing.T) {
	server := server.MustCreateServer(defaultConfig(), nil)
	assert.NotNil(t, server)
}

func TestNewKafkaConsumer(t *testing.T) {
	c, err := server.NewKafkaConsumer(defaultConfig())
	assert.Nil(t, err)
	c.Close()
}

func newTestServer(configuration *config.Config, job *[]MockKafkaMessage) *server.Server {
	s := server.MustCreateServer(configuration, NewMockKafkaConsumer(job))
	return s
}

func simulateRun(t *testing.T, configuration *config.Config, minDuration time.Duration, maxDuration time.Duration, jobs *[]MockKafkaMessage, versions ...MockReleaseEvent) {
	defer os.Remove(configuration.ReleasePath)
	// Fist, we create the release file
	err := CreateReleaseFile(configuration.ReleasePath)
	assert.Nil(t, err)
	go func() {
		err := ModifyReleaseFile(configuration.ReleasePath, versions...)
		assert.Nil(t, err)
	}()
	// Check that the server didn't stop before the expected time or after it
	stopped := atomic.NewBool(false)
	go func() {
		if minDuration > 0 {
			time.Sleep(minDuration)
			assert.False(t, stopped.Load())
		}
		if maxDuration > 0 {
			time.Sleep(maxDuration - minDuration)
			assert.True(t, stopped.Load())
		}
	}()
	//we can finally start the server
	s := newTestServer(configuration, jobs)
	err = s.Run()
	stopped.Store(true)
	assert.Nil(t, err)
}

func TestRun(t *testing.T) {
	configuration := defaultConfig()
	// messages to be consumed
	jobPartition := kafka.TopicPartition{
		Topic: &configuration.Topics[0],
	}
	versions := []MockReleaseEvent{
		{
			Version: server.Version{
				ID:        "test",
				TimeStamp: configuration.ReleaseTime.Add(-10 * time.Hour),
			},
			Sleep: 10 * time.Millisecond,
		},
		{
			Version: server.Version{
				ID:        "test1",
				TimeStamp: configuration.ReleaseTime.Add(time.Hour),
			},
			Sleep: 1000 * time.Millisecond,
		},
	}

	// messages to be consumed for jobs
	jobMessages := []MockKafkaMessage{
		{
			Message: kafka.Message{
				Value:          []byte(`Hello, world!`),
				TopicPartition: jobPartition,
			},
			DelayMS: 1,
		},
		{
			Message: kafka.Message{
				Value:          []byte(`Foo, bar!`),
				TopicPartition: jobPartition,
			},
			DelayMS: 100,
		},
		{
			Message: kafka.Message{
				Value:          []byte(`Foo, bar!`),
				TopicPartition: jobPartition,
			},
			DelayMS: 100,
		},
	}
	simulateRun(t, configuration, 0, 2*time.Second, &jobMessages, versions...)
}

// Test stopping the server while processing messages
func TestRunStopWhileProcessing(t *testing.T) {
	configuration := defaultConfig()
	jobPartition := kafka.TopicPartition{
		Topic: &configuration.Topics[0],
	}
	versions := []MockReleaseEvent{
		{
			Version: server.Version{
				ID:        "test",
				TimeStamp: configuration.ReleaseTime.Add(time.Hour),
			},
			Sleep: 1000 * time.Millisecond,
		},
	}
	// messages to be consumed for jobs
	// The first message will make the server work for 3 seconds,
	// thus terminating after the contorl message is processed
	jobMessages := []MockKafkaMessage{
		{
			Message: kafka.Message{
				Value:          []byte("sleep\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 1,
		},
		{
			Message: kafka.Message{
				Value:          []byte("test\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 100,
		},
		{
			Message: kafka.Message{
				Value:          []byte("Foo, bar!\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 100,
		},
		{
			Message: kafka.Message{
				Value:          []byte(`Foo, bar!\n`),
				TopicPartition: jobPartition,
			},
			DelayMS: 300,
		},
	}
	simulateRun(t, configuration, 2*time.Second, 4*time.Second, &jobMessages, versions...)
}

// Test a delay in incoming jobs doesn't starve workers.
func TestRunDelay(t *testing.T) {
	configuration := defaultConfig()
	defer os.Remove(configuration.ReleasePath)
	jobPartition := kafka.TopicPartition{
		Topic: &configuration.Topics[0],
	}

	versions := []MockReleaseEvent{
		{
			Version: server.Version{
				ID:        "test123",
				TimeStamp: configuration.ReleaseTime.Add(time.Hour),
			},
			Sleep: 5000 * time.Millisecond,
		},
	}
	jobMessages := []MockKafkaMessage{
		{
			Message: kafka.Message{
				Value:          []byte("test\n"),
				TopicPartition: jobPartition,
			},
			// This message will be processed only on the 4th attempt
			DelayMS: 4000,
		},
		{
			Message: kafka.Message{
				Value:          []byte("error\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 500,
		},
		{
			Message: kafka.Message{
				Value:          []byte("Foo, bar!\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 1,
		},
		{
			Message: kafka.Message{
				Value:          []byte(`Foo, bar!\n`),
				TopicPartition: jobPartition,
			},
			DelayMS: 1,
		},
	}
	// Note that while the server will have received a superseding version
	// update, it will still be running the job with error outcome due to
	// our retry with exponential jittered backoff.
	// With the current settings, it will take 3 attempts to fail, which
	// amounts up to 8.25s elapsed time: message processing starts at 4.5s,
	// followed by two retries with an initial interval of 1s, (default)
	// multiplier of 1.5 and (default) randomization factor 0.5.
	// We also apply a small buffer on top of this to account for execution
	// overheads etc.
	simulateRun(t, configuration, 0, 8300*time.Millisecond, &jobMessages, versions...)
}

func TestRetryElapsedTimeLimit(t *testing.T) {
	configuration := defaultConfig()
	// Limit total retry elapsed time to 2s overall.
	configuration.MaxRetryElapsedTime = 2 * time.Second
	defer os.Remove(configuration.ReleasePath)
	jobPartition := kafka.TopicPartition{
		Topic: &configuration.Topics[0],
	}
	versions := []MockReleaseEvent{
		{
			Version: server.Version{
				ID:        "test123",
				TimeStamp: configuration.ReleaseTime.Add(time.Hour),
			},
			Sleep: 1000 * time.Millisecond,
		},
	}
	jobMessages := []MockKafkaMessage{
		{
			Message: kafka.Message{
				Value:          []byte("error\n"),
				TopicPartition: jobPartition,
			},
			DelayMS: 1,
		},
	}
	// Our one job persistently fails. With a retry limit of 2, initial interval
	// of 1s, multiplier of 1.5, and (default) randomization factor of 0.5, that
	// can range from 1250ms to 3750ms. However, with a cap of 2s on the elapsed
	// time, we will never take much longer than that (with a small buffer for
	// execution overheads, etc.).
	simulateRun(t, configuration, 0, 2200*time.Millisecond, &jobMessages, versions...)
}
