#!/bin/bash
# Example script for testing scenarios in mercurius
while IFS= read -r line
do
    if grep -q "error" <<< "$line"; then
        exit 1
    fi
    if grep -q "sleep" <<< "$line"; then
        sleep 3
    fi
    if ! grep -q "exit" <<< "$line"; then
        echo "$line"
    fi
done < /dev/stdin
