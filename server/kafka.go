// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")
package server

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.wikimedia.org/repos/sre/mercurius/config"
)

// Minimal interface needed for testing purposes
type KafkaConsumerApi interface {
	// Close the consumer
	Close() error
	// Subscribe to a topic
	Subscribe(string, kafka.RebalanceCb) error
	// Subscribe multiple topics
	SubscribeTopics([]string, kafka.RebalanceCb) error
	// Poll for messages
	Poll(timeoutMS int) kafka.Event
	// Retreive the current assigned partition topics
	Assignment() ([]kafka.TopicPartition, error)
	// Assign a topic partition
	Assign([]kafka.TopicPartition) error
	// Unsubscribe from a topic
	Unsubscribe() error
}

// NewKafkaConsumer creates a new Kafka consumer
func NewKafkaConsumer(configuration *config.Config) (*kafka.Consumer, error) {
	consumer, err := kafka.NewConsumer(configuration.KafkaConsumerConfig())
	if err != nil {
		return nil, err
	}
	return consumer, nil
}
