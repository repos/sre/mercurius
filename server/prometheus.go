// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")
package server

import (
	"time"

	"gerrit.wikimedia.org/r/operations/software/prometheus-rdkafka-exporter/promrdkafka"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	kafkaMetrics = promrdkafka.NewMetrics()

	kafkaConsumerMessagesCounter = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "mercurius_kafka_consumer_messages",
			Help: "The total number of messages consumed by the Kafka consumer",
		},
		[]string{"topic", "consumer"},
	)
	kafkaConsumerErrorsCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "mercurius_kafka_consumer_errors",
		Help: "The total number of errors encountered by the Kafka consumer"},
		[]string{"topic", "consumer"},
	)
	processedJobsCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "mercurius_processed_jobs",
		Help: "The total number of jobs processed by the server"},
		[]string{"topic", "outcome", "command"},
	)
	jobRetriesCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "mercurius_job_retries",
		Help: "The total number of retries for a job"},
		[]string{"topic", "command"},
	)
	commandFailuresCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "mercurius_job_failures",
		Help: "The total number of jobs that fail completely (after retries)"},
		[]string{"topic", "command", "exit_code"},
	)

	activeWorkersGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "mercurius_active_workers",
		Help: "The current number of workers actively managing jobs (executing or pending retry)"},
		[]string{"topic", "command"},
	)
	serverStatusGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "mercurius_server_status",
		Help: "The current status of this server instance"},
		[]string{"status"},
	)

	jobDurationHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "mercurius_job_duration_seconds",
		Help: "The duration of a job in seconds",
		// Non-uniform human-friendly buckets spanning 500ms to 36h, which
		// roughly matches the range of transcode jobs.
		Buckets: mustParseSeconds("500ms", "1s", "5s", "10s", "30s", "1m", "5m", "10m", "30m", "1h", "3h", "6h", "12h", "24h", "36h")},
		[]string{"topic", "command"},
	)
)

// mustParseSeconds converts the provided duration strings to float64 seconds and panics on error (parse failure).
func mustParseSeconds(strs ...string) []float64 {
	var seconds []float64
	for _, s := range strs {
		d, err := time.ParseDuration(s)
		if err != nil {
			panic(err)
		}
		seconds = append(seconds, d.Seconds())
	}
	return seconds
}
