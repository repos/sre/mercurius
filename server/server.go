// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")
package server

import (
	"context"
	"errors"
	"fmt"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/rs/zerolog/log"
	"gitlab.wikimedia.org/repos/sre/mercurius/config"
)

// Server is the main struct for the mercurius server
type Server struct {
	config        *config.Config
	EventConsumer KafkaConsumerApi
	Version       *Version
}

// MustCreateServer creates a new mercurius server
func MustCreateServer(configuration *config.Config, consumer KafkaConsumerApi) *Server {
	// Check that we have version information from the configuration.
	// if not, we will fail fast.
	if configuration.ReleaseID == "" {
		log.Fatal().Msg("No release ID provided")
		return nil
	}
	if configuration.ReleaseTime.IsZero() {
		log.Fatal().Msg("No release time provided")
		return nil
	}
	version := Version{
		ID:        configuration.ReleaseID,
		TimeStamp: configuration.ReleaseTime,
	}

	return &Server{
		config:        configuration,
		EventConsumer: consumer,
		Version:       &version,
	}
}

func setStatus(status string) {
	serverStatusGauge.Reset()
	serverStatusGauge.With(map[string]string{"status": status}).Set(1)
}

type workItem struct {
	topic   string
	message string
}

// Run starts the server
func (s *Server) Run() error {
	// Mark this instance as initializing.
	setStatus("startup")
	// These variables will hold the error returned by the goroutines.
	// This is preferred to just using an errgroup because we want to
	// have hierarchical control between the goroutines.
	var consumerErr error
	var watcherErr error
	// Sync group to ensure we wait for all goroutines on exit
	var wg sync.WaitGroup
	// Main context for the server. Used to manage the release watcher and the job consumer goroutines.
	ctx, cancel := context.WithCancel(context.Background())

	// We also need a second context, to allow stopping the workers only once
	// the job consumer is done, else we risk stopping the workers before they
	// have a chance to process the last messages.
	workersCtx, workersCancel := context.WithCancel(context.Background())

	// Create two channels: one for delivering messages to the workers
	// and another for signaling the kafka consumer when there are workers
	// available to process messages.
	var work = make(chan *workItem)
	var workersAvailable = make(chan int, s.config.MaxWorkers)
	defer close(work)
	defer close(workersAvailable)
	// Start the control checking thread.
	// It will watch the release info path for changes every second,
	// and it will exit when a new release happens, cancelling the context.
	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Debug().Msg("Starting the version watcher")
		err := s.Version.Watch(ctx, s.config.ReleasePath)
		log.Debug().Msg("Version watcher stopped")
		// If we got here it means we are done, either because of an error
		// or because we received a new version.
		// In any case, we should stop the workers.
		cancel()
		if err != nil {
			watcherErr = err
		}
	}()

	// Start the kafka listeners
	wg.Add(1)
	go func() {
		defer wg.Done()
		// When this goroutine exits, we will stop the workers.
		defer workersCancel()
		// We should also stop the watcher when this goroutine exits.
		defer cancel()
		consumerErr = s.setupEventConsumer()
		if consumerErr != nil {
			return
		}
		log.Debug().Msg("Starting the kafka job consumer")
		consumerErr = s.consumeJobs(ctx, work, workersAvailable)
		// In this case, we shouldn't have stopped consuming unless
		// a) we got an error or b) the context was cancelled
		//    elsewhere.
		log.Debug().Msg("Kafka job consumer stopped")
	}()

	// Start the workers
	for i := 0; i < s.config.MaxWorkers; i++ {
		// We need to capture the value of i in the closure
		id := i
		wg.Add(1)
		// Start a new worker goroutine. Please note that these
		// goroutines will never return unless the workers context is cancelled.
		go func() {
			defer wg.Done()
			log.Debug().Msgf("Starting worker %d", id)
			s.runWorker(workersCtx, work, workersAvailable, id)
			log.Debug().Msgf("Worker %d stopped", id)
		}()
	}
	// Wait for all goroutines to finish.
	// if any of them returns an error, we will return it.
	wg.Wait()
	return errors.Join(consumerErr, watcherErr)
}

// setupEventConsumer sets up the consumer for the job topics
func (s *Server) setupEventConsumer() error {
	err := s.EventConsumer.SubscribeTopics(s.config.Topics, nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed to subscribe to the job topics")
		return err
	}
	return nil
}

// consumeKafka consumes messages from the control and job topics
func (s *Server) consumeJobs(ctx context.Context, work chan *workItem, workersAvailable chan int) error {
	// Ensure we close all connections.
	defer func() {
		s.EventConsumer.Close()
	}()
	// Mark this instance as active on entry and shutting down on return.
	setStatus("active")
	defer setStatus("shutdown")
	promLabels := make(map[string]map[string]string, len(s.config.Topics)+1)
	for _, topic := range s.config.Topics {
		labels := map[string]string{"topic": topic, "consumer": "job"}
		promLabels[topic] = labels
	}
	promLabels["unknown"] = map[string]string{"topic": "unknown", "consumer": "job"}

	// main goroutine loop.
	for {
		// First let's check
		select {
		case <-ctx.Done():
			return nil
		case <-workersAvailable:
			// We have workers available, we can consume one message from the job topic
			ev := s.EventConsumer.Poll(1000)
			if ev == nil {
				workersAvailable <- 1
				continue
			}
			switch e := ev.(type) {
			case *kafka.Message:
				kafkaConsumerMessagesCounter.With(promLabels[*e.TopicPartition.Topic]).Inc()
				log.Debug().Msgf("Received a message on the job topic: %s", *e.TopicPartition.Topic)
				work <- &workItem{
					topic:   *e.TopicPartition.Topic,
					message: string(e.Value),
				}
			case kafka.PartitionEOF:
				// Ignore, free the worker.
				workersAvailable <- 1
			case kafka.Error:
				// In case of a kafka error, we log it, and only stop processing if the error
				// is fatal.
				kafkaConsumerErrorsCounter.With(promLabels["unknown"]).Inc()
				log.Error().Err(e).Msg("Error while consuming a message from the job topics")
				if e.IsFatal() {
					return e
				}
				// if the error wasn't fatal, we just free the worker
				workersAvailable <- 1
			case *kafka.Stats:
				err := kafkaMetrics.Update(e.String())
				if err != nil {
					log.Error().Err(err).Msg("Failed to update the kafka metrics")
				}
				workersAvailable <- 1
			case kafka.OffsetsCommitted:
				if e.Error != nil {
					log.Error().Err(e.Error).Msgf("Error while auto-committing offsets: %s", e.String())
				}
				workersAvailable <- 1
			default:
				// This should never happen, but we log it just in case.
				log.Error().Msgf("Unexpected event type: %T", e)
				workersAvailable <- 1
			}
		}
	}
}

// runWorker processes messages from the messages channel
func (s *Server) runWorker(ctx context.Context, work chan *workItem, workersAvailable chan int, workerId int) {
	// Please note that this function will never return unless the context is cancelled.
	// If you need to add errors that will make it fail here, you then need to add an external
	// for loop in the function calling it to ensure we restart the worker.
	interval := time.Duration(s.config.RetryInterval) * time.Second
	b := backoff.WithMaxRetries(
		backoff.NewExponentialBackOff(backoff.WithInitialInterval(interval), backoff.WithMaxElapsedTime(s.config.MaxRetryElapsedTime)),
		uint64(s.config.MaxRetries),
	)

	// Signal that we are ready to process messages at the start of the loop.
	workersAvailable <- 1
	for {
		select {
		case <-ctx.Done():
			return
		case item := <-work:
			active := activeWorkersGauge.With(map[string]string{"topic": item.topic, "command": s.config.Command})
			active.Inc()
			log.Debug().Msgf("Worker %d: Processing message from topic %s: %s", workerId, item.topic, item.message)
			// We will retry the command MaxRetries times, with an exponential backoff
			// between retries. Please note that we will not stop retrying if we got
			// a cancellation message, as we want to process all messages we've consumed.
			err := backoff.RetryNotify(
				func() error { return s.runCommand(item, workerId) },
				b,
				func(error, time.Duration) {
					jobRetriesCounter.With(map[string]string{"topic": item.topic, "command": s.config.Command}).Inc()
				},
			)
			outcome := "success"

			if err != nil {
				outcome = "failure"
				log.Error().Err(err).Msgf("Worker %d: Failed to execute the command '%s' for message from topic %s with input: %s", workerId, s.config.Command, item.topic, item.message)
			}
			processedJobsCounter.With(map[string]string{"topic": item.topic, "command": s.config.Command, "outcome": outcome}).Inc()
			active.Dec()
			// Signal that we are ready to process messages again.
			workersAvailable <- 1
		}
	}
}

// runCommand runs a command and returns an error if it fails
func (s *Server) runCommand(item *workItem, workerId int) error {
	var exitStatus int
	start := time.Now()
	defer func() {
		jobDurationHistogram.With(map[string]string{"topic": item.topic, "command": s.config.Command}).Observe(time.Since(start).Seconds())
	}()
	cmd := exec.Command(s.config.Command, s.config.CommandArgs...)
	cmd.Stdin = strings.NewReader(item.message)
	out, err := cmd.CombinedOutput()
	log.Debug().Msgf("Command output: %s", string(out))

	if err != nil {
		log.Error().Err(err).Msgf("Worker %d: Failed to execute the command: output '%s'", workerId, string(out))
		switch e := err.(type) {
		case *exec.ExitError:
			exitStatus = e.ExitCode()
		default:
			exitStatus = -1
		}
		commandFailuresCounter.With(map[string]string{"topic": item.topic, "command": s.config.Command, "exit_code": fmt.Sprint(exitStatus)}).Inc()
		return err
	}
	return nil
}
