// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")

package server

import (
	"context"
	"encoding/json"
	"os"
	"time"

	"github.com/rs/zerolog/log"
)

// Version encapsulates the version information
type Version struct {
	ID        string    `json:"id"`
	TimeStamp time.Time `json:"ts"`
}

// VersionFromFile reads the version information from a file
func VersionFromFile(path string) (*Version, error) {
	dataFile, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return VersionFromBytes(dataFile)
}

// VersionFromBytes reads the version information from a byte array
func VersionFromBytes(data []byte) (*Version, error) {
	var version Version
	err := json.Unmarshal(data, &version)
	if err != nil {
		return nil, err
	}
	return &version, nil
}

// Replaces checks if the version we received is newer than the one we have
func (v *Version) Replaces(currentVersion *Version) bool {
	if v.ID == currentVersion.ID {
		log.Debug().Msgf("New version %s is the same as the current one", v.ID)
		return false
	}

	if currentVersion.TimeStamp.After(v.TimeStamp) {
		log.Debug().Msg("Old event discarded: timestamp is older than ours")
		return false
	}
	return true
}

// Watch watches a file for new versions, and returns without errors in case a new version is available
func (v *Version) Watch(ctx context.Context, path string) error {
	ticker := time.NewTicker(500 * time.Millisecond)
	for {
		select {
		case <-ctx.Done():
			log.Debug().Msg("Context cancelled, stopping version watch")
			return nil
		case <-ticker.C:
			log.Debug().Msgf("Checking for new version in %s", path)
			newVersion, err := VersionFromFile(path)
			if err != nil {
				log.Error().Err(err).Msg("Failed to read version information")
				return err
			}
			if newVersion.Replaces(v) {
				log.Debug().Msgf("New version %s replaces the current one %s", newVersion.ID, v.ID)
				return nil
			}
		}
	}
}
