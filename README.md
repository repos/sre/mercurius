# Mercurius

This software is designed to run asynchronous tasks in a distributed system with a central workload orchestrator like Kubernetes.

It is designed with the goal of being able to finish long-running jobs whenever there is a new release of the code, without redeploying everything, and specifically  to handle videoscaling duties at the Wikimedia Foundation. Specifically, we needed a way to run jobs that will last for potentially a long time, even days,
and ensure we run new jobs only with the latest deployed version of the code. While it's perfectly possible to implement this mechanism by submitting jobs the kubernetes API,
we didn't want to put a strain on the kubernets masters given the frequency of the jobs we'd be submitting. Hence the need of an ad-hoc solution.

The software will know the current release ID and timestamp from its configuration, and it will monitor a file, that will be provided by the orchestrator,
to check if a newer version of the software has been released. Whenever that happens, we stop consuming jobs and just finish
the work that's already ongoing. Jobs are consumed from multiple kafka topics and run via a fixed number of workers, allowing
the user to pre-determine the concurrency at which we want to process jobs.

Whenever mercurius stops because of a new version is released, it exits with status code 0, and with a non-zero status code if it crashes due to an error,
allowing the orchestrator to decide if the program should be restarted.

## Configuration
You can change the behaviour of mercurius by defining a yaml-format file, located by default in `~/.mercurius.yaml`, but its location can be defined by passing the `--config` command line flag or by setting the `MERCURIUS_CFG` environment variable.

### Configuration file example
```yaml
# List of brokers to bootstrap from
brokers:
  - localhost:9200
# kafka tls configuration
tls:
  ca-path: ssl/ca.crt
  ciphers: "ECDHE-ECDSA-AES256-GCM-SHA384"
  curves: "P-256"
  sig-algs: "ECDSA+SHA256"

# Kafka topics to consume jobs from
topics:
  - job.topic.a
  - job.topic.b
# The consumer group when consuming the job topics
group: mercurius
# number of parallel workers running
workers: 10
command: "/usr/bin/runJobs.sh"
command-args:
  - "--stdin"
  - "--debug"
# maximum number of retries for a job
max-retries: 3
# time for the first retry in exponential backoff
retry-interval: 10
# arguments to pass to the Kafka consumer
consumer-properties:
  max.poll.interval.ms: 300000
# release information
release-id: "amazing-software:20240101"
# Time of the release. It will be set to the current time if not specified in the configuration
release-time: "2024-01-01T00:00:00Z"
release-path: ~/.mercurius-release.json
```
### command-line flags
Mercurius supports the following command-line flags:
* `--config` to provide an alternative path for the config file
* `--debug` to log debug information
* `--metrics-address` to change the address/port pair on which the prometheus metrics http server is running.

## Mode of operation

When the softare starts, it is given a release ID string, and a timestamp of the release from its configuration.

After starting, it will read every half a second the file configured at `release-path` (by default, `/etc/mercurius/release.json`), that
should look like this:

```json
{
    "id": "mediawiki-multiversion:202401011021",
    "ts": "2024-01-01T00:00:00Z",
}
```
In Kubernetes, this can work as a `Job` you deploy every time your software changes,
together with a configmap that is shared between all the job instances. The release
information will be included in the configmap, and the old `Job` instances will detect
a new version has been released and drain themselves.

If you're using helm, you can implement this mechanism with relative ease -
create a configmap for the mercurius configuration and the release data:
```yaml
{{ $release := .Values.software_version }}
{{ $ts := now | date "2006-01-02T15:04:05Z" }}
---
apiVersion: v1
kind: ConfigMap
# The configmap should be shared by all Job entries.
metadata: ...
data:
  # This file is read by mercurius only on startup
  config.yaml: |-
    release-id: "{{ $release }}"
    release-date: {{ $ts }}
    ...
  # This file is monitored by mercurius for changes
  release.json: |-
    {{ dict "id" $release "ts" ts | toJson | indent 4 }}
```
If mercurius stops because of an error, it will exit with non-zero exit status, and the `Job` in kubernetes should be configured to restart in such cases.
It should exit with status code zero when it's been properly drained so that a new one isn't respawned.
