// Copyright (c) 2024 Giuseppe Lavagetto <joe@wikimedia.org>
// Licensed under the Apache License, Version 2.0 (the "License")
package config

import (
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type KafkaTLSConfig struct {
	CAPath  string `mapstructure:"ca-path"`
	Ciphers string `mapstructure:"ciphers"`
	Curves  string `mapstructure:"curves"`
	SigAlgs string `mapstructure:"sig-algs"`
}

// TODO: import VersionID from env variables
type Config struct {
	Brokers    []string `mapstructure:"brokers"`
	Topics     []string `mapstructure:"topics"`
	Group      string   `mapstructure:"group"`
	MaxWorkers int      `mapstructure:"workers"`
	MaxRetries int      `mapstructure:"max-retries"`
	// MaxRetryElapsedTime is the total elapsed time limit on retry attempts,
	// inclusive of job execution time. A zero duration means no limit.
	MaxRetryElapsedTime time.Duration     `mapstructure:"max-retry-elapsed-time"`
	TLS                 *KafkaTLSConfig   `mapstructure:"tls"`
	ReleaseID           string            `mapstructure:"release-id"`
	ReleasePath         string            `mapstructure:"release-path"`
	ReleaseTime         time.Time         `mapstructure:"release-time"`
	Command             string            `mapstructure:"command"`
	CommandArgs         []string          `mapstructure:"command-args"`
	ConsumerProperties  map[string]string `mapstructure:"consumer-properties"`
	// TODO: make this a time.Duration
	RetryInterval int `mapstructure:"retry-interval"`
}

// KafkaConsumerConfig returns a Kafka configuration object
func (c Config) KafkaConsumerConfig() *kafka.ConfigMap {
	conf := &kafka.ConfigMap{
		"bootstrap.servers":        strings.Join(c.Brokers, ","),
		"group.id":                 c.Group,
		"auto.offset.reset":        "latest",
		"go.events.channel.enable": false,
	}

	for k, v := range c.ConsumerProperties {
		conf.SetKey(k, v)
	}

	if c.TLS != nil {
		conf.SetKey("security.protocol", "SSL")
		conf.SetKey("ssl.ca.location", c.TLS.CAPath)
		conf.SetKey("ssl.cipher.suites", c.TLS.Ciphers)
		conf.SetKey("ssl.curves.list", c.TLS.Curves)
		conf.SetKey("ssl.sigalgs.list", c.TLS.SigAlgs)

	}
	return conf
}
