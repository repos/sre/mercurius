/*
Copyright © 2024 Giuseppe Lavagetto

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"net/http"
	"os"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/mercurius/config"
	"gitlab.wikimedia.org/repos/sre/mercurius/server"
	"go.elastic.co/ecszerolog"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Configuration file location. If present will provide variables to the command line flags
var cfgFile string
var debug bool
var metricsAddress string

// default configuration
var configuration config.Config = config.Config{
	Brokers:             []string{"localhost:9092"},
	Topics:              []string{"job.topic"},
	Group:               "mercurius",
	MaxWorkers:          1,
	ReleaseID:           "some-release-id",
	ReleasePath:         "/etc/mercurius/release.json",
	ReleaseTime:         time.Now(),
	MaxRetries:          3,
	MaxRetryElapsedTime: 0, // Default: No elapsed time limit on retries.
	RetryInterval:       1,
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "mercurius",
	Short: "Consume messages from a Kafka topic and execute a configurable command for each message.",
	Long: `This tool is intended to be used as a daemon that consumes messages from a Kafka topic and,
	for each message, executes a configurable command. The program also watches a file for release information.
	If the release information changes, and the release ID is different from the one provided in the configuration,
	the workers are drained and the program is terminated.
	Finally, a system of retries is also supported, in case the command fails to execute.`,

	Run: func(cmd *cobra.Command, args []string) {
		log.Info().Msg("Starting mercurius")
		log.Debug().Msgf("Configuration: %+v", configuration)
		log.Debug().Msgf("Kafka consumer configuration: %+v", configuration.KafkaConsumerConfig())
		// Serve prometheus metrics under /metrics
		go func() {
			log.Info().Msg("Starting metrics server")
			http.Handle("/metrics", promhttp.Handler())
			http.ListenAndServe(metricsAddress, nil)
			log.Info().Msgf("Metrics server listening on %s", metricsAddress)
		}()
		kafkaConsumer, err := server.NewKafkaConsumer(&configuration)
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to create a kafka consumer")
		}
		if err := server.MustCreateServer(&configuration, kafkaConsumer).Run(); err != nil {
			log.Fatal().Err(err).Msg("Server failed")
		} else {
			log.Info().Msg("Server terminating")
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initLogging, initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.mercurius.yaml)")
	// Dev/debug flags
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "Enable debug logging")
	// Metrics address
	rootCmd.PersistentFlags().StringVar(&metricsAddress, "metrics-address", ":9100", "Address to bind the metrics server to")
}

func initLogging() {
	// Set up logging and tie it to the global logger
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
	log.Logger = ecszerolog.New(os.Stderr)
}

func initConfig() {

	envConfig := os.Getenv("MERCURIUS_CFG")

	// set a different delimiter so that we don't hit issues with
	// dots in variable names used for kafka consumer options
	// https://github.com/spf13/viper/issues/324#issuecomment-640074300
	v := viper.NewWithOptions(viper.KeyDelimiter("::"))

	if envConfig != "" && cfgFile != "" {
		log.Fatal().Msg("Refusing to start server when both --config is supplied and MERCURIUS_CFG is set")
	}

	if envConfig != "" {
		v.SetConfigFile(envConfig)
	} else if cfgFile != "" {
		// Use config file from the flag.
		v.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cobra" (without extension).
		v.AddConfigPath(home)
		v.SetConfigType("yaml")
		v.SetConfigName(".mercurius")
	}

	v.AutomaticEnv()

	if err := v.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("ReadInConfig failed")
	}
	log.Info().Msgf("Using config file: %s", v.ConfigFileUsed())

	hooks := mapstructure.ComposeDecodeHookFunc(
		// Viper defaults:
		mapstructure.StringToTimeDurationHookFunc(),
		mapstructure.StringToSliceHookFunc(","),
		// Viper does not configure a string -> time.Time DecodeHook, which can
		// make for surprises if times are quoted in config. While we could write
		// a more complex hook supporting multiple formats, it's simpler (and more
		// consistent with Time's own unmarshalers) to just support RFC 3339.
		mapstructure.StringToTimeHookFunc(time.RFC3339),
	)
	if err := v.Unmarshal(&configuration, viper.DecodeHook(hooks)); err != nil {
		log.Fatal().Err(err).Msg("Unmarshal failed")
	}
}
